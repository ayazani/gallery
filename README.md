# Pet app - Photo gallery

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Used technologies:
- React with hooks (ver 16+)
- React Router for routing
- {JSON} Placeholder as API (https://jsonplaceholder.typicode.com/)
- TypeScript
- SASS preprocessor
- Redux store (MobX store earlier)
- axios library for API actions

### How to run:
- `npm install` to install all the dependencies
- `npm start` to start in development mode
