import { IAlbum, IState, IPhoto } from "../types"

const addData = (album: IAlbum, photosArray: IPhoto[]): IAlbum => {
    const photos = photosArray.filter(photo => album.id === photo.albumId);
    //console.log(photos);
    return {...album, layot: photos[0].thumbnailUrl, photos: photos.length}
}

 const getPhotosData = (albums: IAlbum[], photosArray: IPhoto[]): IAlbum[] => {
    return (albums.map(album => (
        addData(album, photosArray)
    )))
}
export default getPhotosData;
