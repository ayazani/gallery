import { IActionAlbum, IAlbum, IActionFlag, IActionUser, IUser, IActionPhoto, IPhoto } from  "../../types"
import { Dispatch } from "redux"

export const setAlbums = (albums: IAlbum[]) => {
    return (dispatch: Dispatch <IActionAlbum>) => {
        dispatch({
            type: "setAlbums",
            payload: albums
        })
    }
}

export const setDataIsLoaded = (isDataLoaded: boolean) => {
    return (dispatch: Dispatch <IActionFlag>) => {
        dispatch({
            type: "setDataIsLoaded",
            payload: isDataLoaded
        })
    }
}
export const setUsers = (users: IUser[]) => {
    return (dispatch: Dispatch <IActionUser>) => {
        dispatch({
            type: "setUsers",
            payload: users
        })
    }
}

export const setPhotos = (photos: IPhoto[]) => {
    return (dispatch: Dispatch <IActionPhoto>) => {
        dispatch({
            type: "setPhotos",
            payload: photos
        })
    }
}
