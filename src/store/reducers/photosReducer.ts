import { IActionPhoto, IPhoto } from "../../types"
const InitialState: IPhoto[] = [];
const reducer = (state: IPhoto[] = InitialState, action: IActionPhoto)=>{
    switch (action.type) {
        case "setPhotos":
            return action.payload
        default:
            return state;
    }
}
export default reducer;
