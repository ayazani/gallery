import { IActionFlag } from "../../types"
const InitialState: boolean = false;
const reducer = (state: boolean = InitialState, action: IActionFlag)=>{
    switch (action.type) {
        case "setDataIsLoaded":
            return action.payload
        default:
            return state;
    }
}
export default reducer;
