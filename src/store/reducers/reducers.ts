import { combineReducers } from 'redux'
import albumReducer from "./albumsReducer"
import photosReducer from "./photosReducer";
import usersReducer from "./usersReducer";
import flagReducer from "./flagReducer";

const reducers = combineReducers({
    photos: photosReducer,
    albums: albumReducer,
    users: usersReducer,
    isDataLoaded: flagReducer
})

export default reducers;

export type State = ReturnType<typeof reducers>
