import { IActionUser, IUser } from "../../types"
const InitialState: IUser[] = [];
const reducer = (state: IUser[] = InitialState, action: IActionUser)=>{
    switch (action.type) {
        case "setUsers":
            return action.payload
        default:
            return state;
    }
}
export default reducer;
