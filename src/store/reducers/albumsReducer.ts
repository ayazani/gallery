import {IActionAlbum, IAlbum} from "../../types"

const InitialState: IAlbum[] = [];
const reducer = (state: IAlbum[] = InitialState, action: IActionAlbum)=>{
    switch (action.type) {
        case "setAlbums":
            return action.payload
        default:
            return state;
    }
}
export default reducer;
