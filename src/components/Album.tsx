import React from "react";
import "./Album.scss"

function Album (props: { albumName: string; layout: string; photosQuantity: number, onClick?: Function}) {
    const {albumName, layout, photosQuantity} = props;

    return (
        <>
            <div className="card">
                <div className="imgx">
                    <img src={layout} />
                </div>
                <div className="contentx">
                    <div className="content">
                        <h3>Album: {albumName}</h3>
                        <p>Photos total: {photosQuantity}</p>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Album;

/* return (
        <>
            <div style={{backgroundImage: `url(${layout})`}} className="Album">
                <div className="Label">
                    <label className="Name">Album name: <label className={"content"}>{albumName} </label></label>
                    <label className="Quantity">Photos total: <label className={"content"}>{photosQuantity}</label></label>
                </div>
            </div>
        </>
    ) */
