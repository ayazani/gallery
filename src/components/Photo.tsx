import React from "react";
import { IPhoto } from "../types"
import "./Photo.scss"

function Photo(props: {photo: IPhoto }) {
    const { photo } = props;
    return (
        <div style={{backgroundImage: `url(${photo.url})`}} className={"Photo"}>
        </div>
    )

}
export default Photo;
