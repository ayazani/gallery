import React from "react";
import Modal from 'react-modal'
import { BsChevronCompactLeft } from "react-icons/bs";
import { BsChevronCompactRight } from "react-icons/bs";
import { BsX } from "react-icons/bs";
import "./PhotoModal.scss"


function PhotoModal (props: {onClose: Function, isOpen: boolean, nextPic: Function, prevPic: Function, photoUrl: string, title?: string}) {
    const {onClose, isOpen, nextPic, photoUrl, prevPic, title} = props;

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={()=>{onClose()}}
            ariaHideApp={false}
            className={"Window"}
        >
            <button onClick={()=>{onClose()}} className={"close button"}><BsX/></button>
            <div className={"Modal"}>
                <button onClick={()=>{prevPic()}} className={"navigation button left"}><BsChevronCompactLeft/></button>
                <div className={"Content"}>
                <div className={"picture"}><img src={photoUrl} alt={`${title}`} /></div>
                    <div className={"title"}><label>{title}</label></div>
                </div>
                <button onClick={()=>{nextPic()}} className={"navigation button right"}><BsChevronCompactRight/></button>
            </div>
        </Modal>
    )
}
export default PhotoModal;
