import React, {useContext} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from 'react-router-dom';
import AlbumsListPage from "./pages/AlbumsListPage";
import AlbumPage from "./pages/AlbumPage"
import "./App.scss"

function App() {
  return (
      <div className={"App"}>
      <Router>
        <Switch>
          <Route exact path='/'>
            <AlbumsListPage />
          </Route>
          <Route path='/album/:albumId'>
            <AlbumPage />
          </Route>
        </Switch>
      </Router>
      </div>
  );
}

export default App;
