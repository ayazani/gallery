import React, { useEffect, useState} from "react";
import { useParams } from 'react-router-dom';
import { IAlbumId, IAlbum, IPhoto, IState } from "../types"
import Photo from "../components/Photo"
import PhotoModal from "../components/PhotoModal"
import { Link } from 'react-router-dom';
import { useSelector } from "react-redux"
import ApiService from "../Api/ApiService";

function AlbumPage() {
    const state = useSelector((state: IState)=>state);
    const { albumId } = useParams<IAlbumId>();
    const [photos, setPhotos] = useState(state.photos.filter(photo => photo.albumId === Number(albumId)))
    const [albumInfo, setAlbumInfo] = useState(state.albums.find(album => album.id === Number(albumId)))
    const [isModalOpen, setModalOpen] = useState(false);

    useEffect( ()=> {
        if (!state.photos.length) {
            ApiService.getAlbum(Number(albumId))
                .then(result=>{
                    setAlbumInfo(result.data)
                })
                .catch(error=>{
                    console.log(error)
                })
            ApiService.getAllPhotosFromAlbum(Number(albumId))
                .then(result=>{
                    setPhotos(result.data);
                })
                .catch(error=>{
                    console.log(error)
                })
        }
    })

    const [chosenPhoto, setChosenPhoto] = useState<IPhoto>(photos[0]);

    const openModal = () => {
        setModalOpen(true);
    }

    const closeModal = () => {
        setModalOpen(false);
    }

    const nextPhoto = () => {
        const newIndex = photos.findIndex(photo => photo.id === chosenPhoto.id)+1;
        newIndex > photos.length-1 ? setChosenPhoto(photos[newIndex-1]) : setChosenPhoto(photos[newIndex]);
    }

    const prevPhoto = () => {
        const newIndex = photos.findIndex(photo => photo.id === chosenPhoto.id)-1;
        newIndex < 0 ? setChosenPhoto(photos[newIndex+1]) : setChosenPhoto(photos[newIndex]);
    }

    return (
        <>
            <div className={"backToMainPage"}> <Link to="/" className={"backToMainPage"}>ᐊ Back to albums </Link> </div>
            <ul className={"List"}>
            {photos.map(photo => (
                <li className={"element"} key={photo.id} onClick={
                    ()=>{
                        setChosenPhoto(photo);
                        openModal();
                    }
                }>
                <Photo photo={photo} />
                </li>
            ))}
            </ul>
            {isModalOpen?
                <PhotoModal isOpen={isModalOpen}
                            onClose={closeModal}
                            photoUrl={chosenPhoto.url}
                            nextPic={nextPhoto}
                            prevPic={prevPhoto}
                            title={chosenPhoto.title}
                />
                : null}
        </>
    )
}
export default AlbumPage;
