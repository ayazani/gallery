import React, { useEffect, useState } from "react";
import Album from "../components/Album";
import ApiService from "../Api/ApiService";
import "./AlbumListPage.scss"
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux"
import { bindActionCreators } from "redux"
import * as ActionCreators from "../store/action-creators/AC"
import { IState } from "../types";
import flagReducer from "../store/reducers/flagReducer";
import getPhotosData from "../store/functions"

function AlbumListPage() {
    const state = useSelector((state: IState)=>state);
    const dispatch = useDispatch();

    const actionCreators = bindActionCreators(ActionCreators, dispatch);

    useEffect(()=> {
        if( !!(state.albums.length && state.users.length && state.photos.length)) {
            actionCreators.setDataIsLoaded(true);
        }
        if (!state.isDataLoaded) {
            ApiService.getAllPhotos()
                .then(result => {
                    actionCreators.setPhotos(result.data) // redux
                    return new Promise((resolve => {
                        resolve(
                            ApiService.getAllAlbums()
                                .then(result => {
                                    actionCreators.setAlbums(getPhotosData(result.data, state.photos)) // redux
                                })
                                .catch(error => {
                                    console.log(error);
                                })
                        )
                    }))
                })
                .catch(error => {
                    console.log(error);
                });
            ApiService.getUsers()
                .then(result => {
                    actionCreators.setUsers(result.data) //redux
                })
                .catch(error => {
                    console.log(error)
                })
        }
    })

    const render = () => {
        return(
            <>
            <h1 className={"Heading gallery"}>Photo gallery</h1>
                {state.users.map(user =>
                        <div className={'AlbumsPacks'}>
                            <div>
                                <h2 className={"Heading"}>Author: </h2>
                                <div key={"username"}>Username: <label className={"content"}>{user.username}</label></div>
                                <div key={"email"}>Email: <label className={"content"}>{user.email} </label> </div>
                            </div>
                            <ul className={"List"}>
                                {(state.albums.filter(album => album.userId === user.id).map(album => {
                                    const { title, layot, photos, id} = album;
                                    return (
                                        <li key={`li${id}`} className={"album"}>
                                            <Link to={`/album/${id}`} className={'link'}>
                                            <Album
                                                key={id}
                                                albumName={title}
                                                layout={String(layot)}
                                                photosQuantity={Number(photos)}
                                            />
                                            </Link>
                                        </li>
                                    );
                                }))}
                            </ul>
                    </div>
                )}

            </>
        )
    }

    return (
        render()
    );

}
export default AlbumListPage;
