export interface IAlbum {
    layot?: string;
    title: string;
    photos?: number;
    userId: number;
    id: number;
}
export interface IPhoto {
    albumId: number;
    id: number;
    title: string;
    url: string;
    thumbnailUrl: string;
}

export interface IUser {
    id: number;
    username: string;
    email: string;
}

export interface IAlbumId {
    albumId: string
}

export interface IActionAlbum {
    type: string;
    payload: IAlbum[]
}

export interface IActionFlag {
    type: string;
    payload: boolean
}

export interface IActionUser {
    type: string;
    payload: IUser[]
}

export interface IActionPhoto {
    type: string;
    payload: IPhoto[]
}

export interface IState {
    albums: IAlbum[],
    users: IUser[],
    photos: IPhoto[],
    isDataLoaded: boolean
}
