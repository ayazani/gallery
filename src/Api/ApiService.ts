import api from "./api";

class ApiService {

    getAllAlbums(): Promise<any> {
        return api.get('/albums')
    }

    getAlbum(id: number){
        return api.get(`/albums/${id}`)
    }

    getAlbumsByAuthor(userId: number) {
        return api.get(`albums?userId=${userId}`)
    }

    getAllPhotos() {
        return api.get('/photos')
    }

    getAllPhotosFromAlbum(id: number) {
        return api.get(`/photos?albumId=${id}`)
    }

    getUsers() {
        return api.get('/users');
    }

    getUserById(id: number) {
        return api.get(`/users/${id}`)
    }
}
export default new ApiService();
